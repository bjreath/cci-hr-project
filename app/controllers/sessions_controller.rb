class SessionsController < ApplicationController
  
  def new
  end
  
  def create
    user = User.find_by_email(params[:session][:email])
    if user && user.authenticate(params[:session][:password])
      # Sign in user and redirect to user's show page. (users/:id)
      sign_in user
      redirect_to user
    else
      flash.now[:error] = 'Invalid email/password combination' 
    end
  end

  def destroy
  end
end
